## Architecture consideration
Currently no wallet access security exists (does not check for right to wallet - ex. session token)
## Assumptions
wallets will have a unique ID (uuid)

## usage


    GET /api/wallet/{id}/balance

    POST /api/wallet/{id}/transfer
        parameters:
        {id} - String (32) Recipient wallet uuid
        JSON varibles:
        recipent - String (32) Recipient wallet uuid
        amount - float Amount to transfer

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
