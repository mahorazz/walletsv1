<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class WalletSeeder extends Seeder
{
    /**
     * Run the database seeds. DEVELOPMENT ONLY
     *
     * @return void
     */
    public function run()
    {
        //flush previous data
        DB::table('wallets')->truncate();

        //insert 2 test wallets
        DB::table('wallets')->insert([
            'id' => '076e85aa-53cd-4b44-9fbb-490d526508f4',
            'owner' => 'User_1',
            'balance' => 100.0
        ]);
        DB::table('wallets')->insert([
            'id' => '46e2dddb-ace5-4379-ba3e-960d5475a7bc',
            'owner' => 'User_2',
            'balance' => 200.0
        ]);
    }
}
