<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model{
    //commision fee percentage
    private static $fee = 0.015;
    //calculate transaction commission fee
    public static function commission($amount){
        return $amount * self::$fee;
    }

    protected $fillable = [
        'sender', 'recipient', 'amount'
    ];

    protected $hidden = [];

}