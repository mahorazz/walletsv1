<?php

namespace App\Http\Controllers;

use App\Wallet;
use App\Transaction;
use Illuminate\Http\Request;

class WalletController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //create new wallet
    public function addWallet(Request $request){
        $newWallet = new Wallet();
        $newWallet->owner = $request->owner;
        if($newWallet->save()){
            return response()->json($newWallet, 201);
        }
        else{
            return response()->json(['Error'=>'Wallet could not be created'], 500);
        } 
    }

    public function deposit($uuid, Request $request){
        $wallet = Wallet::where('id',$uuid)->first();
        if($wallet){
            $wallet->balance += $request->amount;
            $wallet->save();
            return response()->json(['new balance' => $wallet->balance], 200);
        }
        else {
            return response()->json(['Error'=>'Wallet not found'], 404);
        }
    }

    //retrieve the balance of the given wallet
    //TODO: add "wallet not found"
    public function balance($uuid){
        $wallet = Wallet::where('id',$uuid)->first();
        if($wallet){
            return response()->json(['balance' => $wallet->balance], 200);
        }
        else {
            return response()->json(['Error'=>'Wallet not found'], 404);
        }
    }

    //relay transaction to transfer service
    public function transfer($uuid, Request $request){
        $amount = $request->amount;
        $commission = Transaction::commission($amount);

        //find the wallet by uuid
        $sender = Wallet::where('id', $uuid)->first();
        
        //check balance + commision fee
            if($sender->balance >= $amount + $commission){
            //fill out a transfer
                //fetch sender wallet
                $newTransfer = new Transaction;
                $newTransfer->sender = $sender->id;
                
                //find the recipient and prepare for transfer
                    //TODO: what do do if recipent wallet does not exist
                $recipient = Wallet::where('id', $request->recipient)->firstOrFail();
                $newTransfer->recipient = $recipient->id;
                $newTransfer->amount = $amount;
                //transfer the funds and save the transfer history
                $sender->balance -= $amount + $commission; 
                $recipient->balance += $amount;
                $sender->save();
                $recipient->save();
                $newTransfer->save();

                return response()->json($newTransfer, 201);
            }
            else{
               //error message
               return response()->json(['Error'=>'Transfer unsuccessful - not enough balance'], 203); 
            }

            
    }
}
