<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model{

    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            if(!$model->getKey()){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function getKeyType(){
        return 'string';
    }

    public function getIncrementing(){
        return false;
    }

    protected $fillable = [
        'owner','balance'
    ];

    protected $hidden = [];

}