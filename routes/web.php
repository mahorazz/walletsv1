<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['prefix' => 'api'], function() use ($router){
    $router->get('/wallet/{uuid}/balance', ['uses' => 'WalletController@balance']);
    $router->put('/wallet/{uuid}/deposit', ['uses' => 'WalletController@deposit']);
    //$router->put('/wallet/{uuid}/withdraw', ['uses' => 'WalletController@withdraw']);
    $router->post('/wallet', ['uses' => 'WalletController@addWallet']);
    
    //data - target, amount
    $router->post('/wallet/{uuid}/transfer', ['uses' => 'WalletController@transfer']);
    //data source, target, amount
    //$router->get('/transfer', ['uses' => 'WalletController@balance']);


});


$router->get('/', function () use ($router) {
    return $router->app->version();
});
